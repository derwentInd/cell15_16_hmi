﻿// Keep these lines for a best effort IntelliSense of Visual Studio 2017 and higher.
/// <reference path="../../Packages/Beckhoff.TwinCAT.HMI.Framework.12.742.5/runtimes/native1.12-tchmi/TcHmi.d.ts" />
(function (/** @type {globalThis.TcHmi} */ TcHmi) {
    var Functions;
    (function (/** @type {globalThis.TcHmi.Functions} */ Functions) {
        var hmi;
        (function (hmi) {
            function RequestPageChange(Content, Region) {

                var region = Region.read();
                region.setTargetContent(Content);

            }
            hmi.RequestPageChange = RequestPageChange;
        })(hmi = Functions.hmi || (Functions.hmi = {}));
        Functions.registerFunctionEx('RequestPageChange', 'TcHmi.Functions.hmi', hmi.RequestPageChange);
    })(Functions = TcHmi.Functions || (TcHmi.Functions = {}));
})(TcHmi);
