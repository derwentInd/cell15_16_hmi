﻿// Keep these lines for a best effort IntelliSense of Visual Studio 2017 and higher.
/// <reference path="../../Packages/Beckhoff.TwinCAT.HMI.Framework.12.742.5/runtimes/native1.12-tchmi/TcHmi.d.ts" />
(function (/** @type {globalThis.TcHmi} */ TcHmi) {
    var Functions;
    (function (/** @type {globalThis.TcHmi.Functions} */ Functions) {
        var hmi;
        (function (hmi) {
            function TestRegularExpression(StringToTest, RegExpRule) {

                if (RegExp == '') {
                    return true;
                }

                var reg = new RegExp(RegExpRule);
                var result = reg.test(StringToTest);
                return result;

            }
            hmi.TestRegularExpression = TestRegularExpression;
        })(hmi = Functions.hmi || (Functions.hmi = {}));
        Functions.registerFunctionEx('TestRegularExpression', 'TcHmi.Functions.hmi', hmi.TestRegularExpression);
    })(Functions = TcHmi.Functions || (TcHmi.Functions = {}));
})(TcHmi);
