﻿// Keep these lines for a best effort IntelliSense of Visual Studio 2017 and higher.
/// <reference path="../../Packages/Beckhoff.TwinCAT.HMI.Framework.12.742.5/runtimes/native1.12-tchmi/TcHmi.d.ts" />
(function (/** @type {globalThis.TcHmi} */ TcHmi) {
    var Functions;
    (function (/** @type {globalThis.TcHmi.Functions} */ Functions) {
        var hmi;
        (function (hmi) {
            function ComboBoxHasSelection(Combo) {

                if (Combo.getSelectedIndex() === null) { return false; }
                if (Combo.getSelectedIndex() === "undefined") { return false; }

                return true;

            }
            hmi.ComboBoxHasSelection = ComboBoxHasSelection;
        })(hmi = Functions.hmi || (Functions.hmi = {}));
        Functions.registerFunctionEx('ComboBoxHasSelection', 'TcHmi.Functions.hmi', hmi.ComboBoxHasSelection);
    })(Functions = TcHmi.Functions || (TcHmi.Functions = {}));
})(TcHmi);
