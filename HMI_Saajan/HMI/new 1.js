var self = TcHmi.Symbol.readEx('%ctrl%NumericKeyboard%/ctrl%');
var value = self.GetValueControl();
var keypad = self.GetNumericKeypadControl();
var code = keypad.getPressedKeys()[0].code;

switch(code) {
  case 'PlusMinus':

		// invert the value
		if (!isNaN(Number(value.getText()))){
			value.setText(Number(value.getText())*-1.0);
		}
		
    break;
    
  case 'NumpadEnter':
    	
    	var submittedValue = Number(value.getText());
    	
    	if (isNaN(submittedValue)){console.log('isNan'); break;}
    	
    	var decimalPlaces = self.GetDecimalPlaces();
    	submittedValue = Number(submittedValue.toFixed(decimalPlaces));
    	
    	TcHmi.Symbol.writeEx('%pp%Value%/pp%', submittedValue);
    	TcHmi.Symbol.writeEx('%pp%DirtyFlag%/pp%', true);
    	
    	if (self.GetAutoClose()) {
    		// required to prevent keyboard accumulating key presses
    		setTimeout(function(){ self.ResetAllValues(); }, 200);
    	}
    break;
}